<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "online_course";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM courses WHERE teach_by IS NULL";
$result = $conn->query($sql);

if($result->num_rows > 0) {
    $count_num = 1;
    echo "List of subjects with do not have instructor<br>";
    while($row = $result->fetch_assoc()) {
        echo $count_num . ". " . $row["name"];
        echo "<br>";
        $count_num++;
    }
}


$conn->close();
?>