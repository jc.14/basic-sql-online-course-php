<!DOCTYPE html>
<html>
<head>
<style>
    table, th, td {
        border: 1px solid black;
        text-align: center;
    }
    th {
        background-color: #98bfe3;
    }
    table {
        display: table-cell;
    }
</style>
</head>

<body>

<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "online_course";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

/*Check which course was enrolled*/
$sql_e = "SELECT DISTINCT courses.name FROM enrolls JOIN courses on enrolls.course_id = courses.id";
$result_e = $conn->query($sql_e);

/*Check which course was not enrolled*/
$sql_u = "SELECT DISTINCT courses.name FROM courses LEFT JOIN enrolls on enrolls.course_id = courses.id WHERE enrolls.course_id IS NULL";
$result_u = $conn->query($sql_u);

displayTable($result_e,'enrolled');
displayTable($result_u,'unenrolled');

function displayTable($result,$topic) {
    if($result->num_rows > 0) {
        echo "<table><tr><th>Lists of " . $topic . " courese</th></tr>";
        while($row = $result->fetch_assoc()) {
            echo "<tr><td>" . $row["name"] . "</td></tr>";
        }
        echo "</table>";
    } else {
        echo "Error: " . $conn->error;
    }    
}

$conn->close();
?>

</body>
</html>