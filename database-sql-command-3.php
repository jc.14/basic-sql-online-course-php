<!DOCTYPE html>
<html>
<head>
<style>
    table, th, td {
        border: 1px solid black;
        text-align: center;
    }
    th {
        background-color: #98bfe3;
    }
    table {
        display: table-cell;
    }
</style>
</head>

<body>

<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "online_course";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

/*search which course was enrolled*/
$sql_e = "SELECT DISTINCT courses.name as title, instructors.name AS ins , courses.price FROM enrolls JOIN courses on enrolls.course_id = courses.id
        JOIN instructors ON courses.teach_by = instructors.id";
$result_e = $conn->query($sql_e);

/*search which course was not enrolled, and with instructor*/
$sql_u = "SELECT DISTINCT courses.name as title , (SELECT instructors.name FROM instructors WHERE instructors.id = courses.teach_by) AS ins 
        , courses.price FROM courses LEFT JOIN enrolls on courses.id = enrolls.course_id WHERE enrolls.course_id IS NULL AND courses.teach_by IS NOT NULL";
$result_u = $conn->query($sql_u);

displayTable($result_e,'enrolled');
displayTable($result_u,'unenrolled');

function displayTable($result,$topic) {
    if($result->num_rows > 0) {
        echo "<table><tr><th colspan=3>Lists of " . $topic . " courese</th></tr>";
        echo "<tr><th>Title</th><th>Instructors</th><th>Price</th></tr>";
        while($row = $result->fetch_assoc()) {
            echo "<tr><td>" . $row["title"] . "</td><td>" . $row["ins"] . "</td><td>" . $row["price"] . "</td></tr>";
        }
        echo "</table>";
    } else {
        echo "Error: " . $conn->error;
    }    
}

$conn->close();
?>

</body>
</html>