<!DOCTYPE html>
<html>
<head>
<style>
    table, th, td {
        border: 1px solid black;
    }

    th {
        background-color: #98bfe3;
    }

    .table_d {
        display: table-cell;
    }
</style>
</head>

<body>

<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "online_course";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

/*User can display data from any desired table.*/

callTable("courses");
callTable("instructors");
callTable("students");

//function for retrive data from table - para(tablename)
function callTable($tname) {
    $sql = "SELECT * FROM $tname";
    $result = $GLOBALS['conn']->query($sql);
    
    if($result->num_rows > 0) {
        $tbAr = $result->fetch_assoc();
        $arKey = array_keys($tbAr);
        $countAr = count($arKey);

        echo "<table class='table_d'><tr><th colspan='6'>" . strtoupper($tname) . "</th></tr>";
        echo "<tr>";
        for($i=0;$i<$countAr;$i++) {
            echo "<th>" . $arKey[$i] . "</th>";
        }
        echo "</tr>";

        //reset the fetch result
        mysqli_data_seek($result,0);

        while($row = $result->fetch_assoc()) {
            for($j=0; $j<$countAr ; $j++) {
                echo "<td>" . $row[$arKey[$j]] . "</td>";
            }
            echo "</tr>";
        }

        echo "</table>";
    }
    else {
        echo $conn->error;
    }
}

//Find the instructors who do not teacg any subject.
avalIns();

function avalIns() {
    $sql = "SELECT instructors.name FROM instructors LEFT JOIN courses ON instructors.id = courses.teach_by WHERE courses.teach_by IS NULL";
    $result = $GLOBALS['conn']->query($sql);

    if($result->num_rows > 0) {
        echo "<table><tr><th>Lists of available instructors</th></tr>";
        $count_num = 1;
        while($row = $result->fetch_assoc()) {
            echo "<tr><td>" . $count_num . ". " . $row["name"] . "</td></tr>";
            $count_num++;
        }
        echo "</table>";
    } else {
        echo "Every lecturers are busy!";
    }
}

//Search for any course that with out instructors
searchCourse();

function searchCourse() {
    $sql = "SELECT * FROM courses WHERE teach_by IS NULL";
    $result = $GLOBALS['conn']->query($sql);

    if($result->num_rows > 0) {
        $count_num = 1;
        echo "<table><tr><th>List of subjects with do not have instructor</th></tr>";
        while($row = $result->fetch_assoc()) {
            echo "<tr><td>" . $count_num . ". " . $row["name"] . "</td></tr>";
            $count_num++;
        }
        echo "</table>";
    }
}


$conn->close();
?>

</body>
</html>