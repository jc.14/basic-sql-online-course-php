<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "online_course";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

/*Find any lecturer who does not teach any subject.*/
$sql = "SELECT instructors.name FROM instructors LEFT JOIN courses ON instructors.id = courses.teach_by WHERE courses.teach_by IS NULL";
$result = $conn->query($sql);


if($result->num_rows > 0) {
    echo "Lists of available instructors<br>";
    $count_num = 1;
    while($row = $result->fetch_assoc()) {
        echo $count_num . ". " . $row["name"] . "<br>";
        $count_num++;
    }
} else {
    echo "Every lecturers are busy!";
}

$conn->close();
?>