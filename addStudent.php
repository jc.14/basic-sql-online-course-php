<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "online_course";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

/*Add student records*/
$sql = "INSERT INTO students (name) VALUE ('JC');";
$sql .= "INSERT INTO students (name) VALUE ('Ko');";
$sql .= "INSERT INTO students (name) VALUE ('Pikky');";
$sql .= "INSERT INTO students (name) VALUE ('Luis');";
$sql .= "INSERT INTO students (name) VALUE ('Jimmy');";
$sql .= "INSERT INTO students (name) VALUE ('Joong');";
$sql .= "INSERT INTO students (name) VALUE ('Golf');";
$sql .= "INSERT INTO students (name) VALUE ('Jarb');";
$sql .= "INSERT INTO students (name) VALUE ('Dong');";
$sql .= "INSERT INTO students (name) VALUE ('Ton')";

if($conn->multi_query($sql) === TRUE) {
    echo "New records created successfully";
} else {
    echo "Error: " . $conn->error;
}

$conn->close();
?>